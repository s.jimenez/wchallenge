/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service.implement;

import com.google.common.base.Objects;
import com.technical.test.wchallenge.backend.dto.UserDTO;
import com.technical.test.wchallenge.backend.service.UserService;
import com.technical.test.wchallenge.backend.utils.RouteConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author jhoan
 */
@Service
@Slf4j
public class UserServiceImp implements UserService{

    @Autowired
    private RestTemplate restTemplate;
    
    @Override
    public UserDTO[] getAllUsers() {
        log.info("Entry into service function to obtain all users");
        ResponseEntity<UserDTO[]> response = restTemplate
                .getForEntity(RouteConstants.URL_USER, UserDTO[].class);
        return response.getBody();
    }

    @Override
    public UserDTO getUserById(Integer userId) {
        log.info("Entry into service function to obtain an user by id");
        ResponseEntity<UserDTO> response = restTemplate
                .getForEntity(RouteConstants.URL_USER + "/" + userId,
                        UserDTO.class);
        return response.getBody();
    }

    @Override
    public UserDTO createUser(UserDTO newUser) {
        log.info("Entry into service function to create a new user");
        ResponseEntity<UserDTO> response = restTemplate
                .postForEntity(RouteConstants.URL_USER, newUser, UserDTO.class);
        return response.getBody();
    }

    @Override
    public Boolean updateUser(Integer userId, UserDTO newUser) {
        log.info("Entry into service function to update an user");
        boolean flag = false;
        String updateURL = RouteConstants.URL_USER + "/" + newUser.getId();
        if (Objects.equal(newUser.getId(), userId)) {
            restTemplate.put(updateURL, newUser);
            flag = true;
        }
        return flag;
    }

    @Override
    public void deleteUser(Integer userId) {
        log.info("Entry into service function to delete an user by id");
        String deleteURL = RouteConstants.URL_USER + "/" + userId;
        restTemplate.delete(deleteURL);
    }

}
