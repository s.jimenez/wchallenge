/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service.implement;

import com.technical.test.wchallenge.backend.dto.CommentDTO;
import com.technical.test.wchallenge.backend.dto.PostDTO;
import com.technical.test.wchallenge.backend.service.CommentService;
import com.technical.test.wchallenge.backend.service.PostService;
import com.technical.test.wchallenge.backend.utils.RouteConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author jhoan
 */
@Service
@Slf4j
public class CommentServiceImp implements CommentService {

    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private PostService postService;
    
    @Override
    public CommentDTO[] getAllComments() {
        log.info("Entry into service function to get all comments");
        ResponseEntity<CommentDTO[]> response = restTemplate
                .getForEntity(RouteConstants.URL_COMMENT, CommentDTO[].class);
        return response.getBody();
    }
    
    @Override
    public CommentDTO[] getCommentsByPost(Integer postId) {
        log.info("Entry into service function to get all comments associated"
                + " with the post id");
        ResponseEntity<CommentDTO[]> response = restTemplate
                .getForEntity(RouteConstants.URL_COMMENT_PARAM + postId,
                        CommentDTO[].class);
        return response.getBody();
    }
    
    @Override
    public ArrayList<CommentDTO> getCommentsByName(String name) {
        log.info("Entry into service function to get all comments associated "
                + "with the name");
        ArrayList<CommentDTO> listComment = new ArrayList<>();
        ResponseEntity<CommentDTO[]> response = restTemplate
                .getForEntity(RouteConstants.URL_COMMENT, CommentDTO[].class);
        for (CommentDTO comment: response.getBody()) {
            if (comment.getName().contains(name)){
                listComment.add(comment);
            }
        }
        return listComment;
    }
    
    @Override
    public ArrayList<CommentDTO> getCommentsByUser(Integer userId) {
        log.info("Entry into service function to get all comments by user id");
        ArrayList<CommentDTO> commentsByUser = new ArrayList<>();
        PostDTO[] postByUser = postService.getPostByUser(userId);
        for (PostDTO post : postByUser) {
            CommentDTO[] commentsByPost = getCommentsByPost(post.getId());
            commentsByUser.addAll(Arrays.asList(commentsByPost));
        }
        return commentsByUser;
    }

    @Override
    public CommentDTO createComment(CommentDTO newComment) {
        log.info("Entry into service function to create a new comment");
        ResponseEntity<CommentDTO> response = restTemplate
                .postForEntity(RouteConstants.URL_COMMENT, newComment,
                        CommentDTO.class);
        return response.getBody();
    }

    @Override
    public Boolean updateComment(Integer commentId, CommentDTO newComment) {
        log.info("Entry into service function to update a comment");
        boolean flag = false;
        String updateURL = RouteConstants.URL_COMMENT + "/" +
                commentId;
        restTemplate.put(updateURL, newComment);
        if (Objects.equals(commentId, newComment.getId())) {
            restTemplate.put(updateURL, newComment);
            flag = true;
        }
        return flag;
    }

    @Override
    public void deleteComment(Integer commentId) {
        log.info("Entry into service function to delete a comment by id");
        String deleteURL = RouteConstants.URL_COMMENT + "/" + commentId;
        restTemplate.delete(deleteURL);
    }

}
