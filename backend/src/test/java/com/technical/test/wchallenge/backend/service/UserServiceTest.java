/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service;

import com.technical.test.wchallenge.backend.dto.AddressDTO;
import com.technical.test.wchallenge.backend.dto.CompanyDTO;
import com.technical.test.wchallenge.backend.dto.GeoDTO;
import com.technical.test.wchallenge.backend.dto.UserDTO;
import com.technical.test.wchallenge.backend.service.implement.UserServiceImp;
import com.technical.test.wchallenge.backend.utils.RouteConstants;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author jhoan
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private UserServiceImp userService;    
    
    @Test
    public void testGetAllUsers() {
        UserDTO[] listUser = new UserDTO[1];
        when(restTemplate.getForEntity(RouteConstants.URL_USER,
                UserDTO[].class))
                .thenReturn(new ResponseEntity<>(listUser,HttpStatus.FOUND));
        UserDTO[] result = userService.getAllUsers();
        assertNotNull(result);
        assertEquals(result.length, listUser.length);
    }

    @Test
    public void testGetUserById() {
        UserDTO saveUser = new UserDTO();
        Integer userId = 1;
        when(restTemplate.getForEntity(RouteConstants.URL_USER + "/" + userId,
                UserDTO.class))
                .thenReturn(new ResponseEntity<>(saveUser,HttpStatus.FOUND));
        UserDTO result = userService.getUserById(userId);
        assertNotNull(result);
        assertEquals(result, saveUser);
    }
    
    @Test
    public void testCreateUser() {
        UserDTO newUser = new UserDTO();
        when(restTemplate.postForEntity(RouteConstants.URL_USER, newUser,
                UserDTO.class))
                .thenReturn(new ResponseEntity<>(newUser, HttpStatus.CREATED));
        UserDTO result = userService.createUser(newUser);
        assertNotNull(result);
        assertEquals(result, newUser);
    }
    
    @Test
    public void testUpdateUser() {
        CompanyDTO newCompany = new CompanyDTO("nn", "nn", "nn");
        CompanyDTO setCompany = new CompanyDTO();
        setCompany.setBs(newCompany.getBs());
        setCompany.setCatchPhrase(newCompany.getCatchPhrase());
        setCompany.setName(newCompany.getName());
        GeoDTO newGeo = new GeoDTO("nn", "nn");
        GeoDTO setGeo = new GeoDTO();
        setGeo.setLat(newGeo.getLat());
        setGeo.setLng(newGeo.getLng());
        AddressDTO newAddress = new AddressDTO("nn", "nn", "nn", "nn", newGeo);
        AddressDTO setAddress = new AddressDTO();
        setAddress.setCity(newAddress.getCity());
        setAddress.setGeo(newAddress.getGeo());
        setAddress.setStreet(newAddress.getStreet());
        setAddress.setSuite(newAddress.getSuite());
        setAddress.setZipcode(newAddress.getZipcode());
        UserDTO updateUser = new UserDTO(1, "nn", "nn", "nn", newAddress, "nn",
                "nn", newCompany);
        UserDTO setUser = new UserDTO();
        setUser.setAddress(updateUser.getAddress());
        setUser.setCompany(updateUser.getCompany());
        setUser.setEmail(updateUser.getEmail());
        setUser.setId(updateUser.getId());
        setUser.setName(updateUser.getName());
        setUser.setPhone(updateUser.getPhone());
        setUser.setUsername(updateUser.getUsername());
        setUser.setWebsite(updateUser.getWebsite());
        Integer userId = 1;
        restTemplate.put(RouteConstants.URL_USER + "/" + userId, updateUser);
        userService.updateUser(userId, updateUser);
        assertNotNull(updateUser);
    }
    
    @Test
    public void testDeleteUser() {
        UserDTO deleteUser = new UserDTO();
        Integer userId = 1;
        restTemplate.delete(RouteConstants.URL_USER + "/" + userId, deleteUser);
        userService.deleteUser(userId);
        assertNotNull(deleteUser);
    }

}
