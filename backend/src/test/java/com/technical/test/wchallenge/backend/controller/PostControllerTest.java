/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.controller;

import com.technical.test.wchallenge.backend.dto.PostDTO;
import com.technical.test.wchallenge.backend.service.PostService;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author jhoan
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PostControllerTest {

    @Mock
    private PostService postService;
    
    @InjectMocks
    private PostController postController;

    @Test
    public void testGetAllPostsNotFound() {
    when(postService.getAllPost()).thenReturn(null);
        ResponseEntity<PostDTO[]> postCon = postController.getAllPosts();
        assertNotNull(postCon);
    }
    
    @Test
    public void testGetAllPostsFound() {
        ArrayList<PostDTO> arrayPost = new ArrayList<>();
        PostDTO[] listPost = new PostDTO[1];
        PostDTO newPost = new PostDTO(1, 1, "sebas", "dark");
        arrayPost.add(newPost);
        arrayPost.toArray(listPost);
        when(postService.getAllPost()).thenReturn(listPost);
        ResponseEntity<PostDTO[]> postCon = postController.getAllPosts();
        assertEquals(postCon.getBody().length, listPost.length);
    }

    @Test
    public void testCreatePostNull() {
        PostDTO newPost = new PostDTO();
        when(postService.createPost(newPost)).thenReturn(null);
        ResponseEntity<PostDTO> postCon = postController.createPost(newPost);
        assertNull(postCon.getBody());
    }
    
    @Test
    public void testCreatePostNotNull() {
        PostDTO newPost = new PostDTO(1, 1, "sebas", "dark");
        when(postService.createPost(newPost)).thenReturn(newPost);
        ResponseEntity<PostDTO> postCon = postController.createPost(newPost);
        assertEquals(postCon.getBody(), newPost);
    }

    @Test
    public void testUpdatePostOk() {
        boolean flag = true;
        PostDTO newPost = new PostDTO();
        newPost.setId(1);
        Integer albumId = 1;
        when(postService.updatePost(albumId, newPost)).thenReturn(flag);
        ResponseEntity<PostDTO> postCon = postController
                .updatePost(albumId, newPost);
        assertNotNull(postCon);
        assertTrue(flag);
    }
    
    @Test
    public void testUpdatePostBadRequest() {
        boolean flag = false;
        PostDTO newPost = new PostDTO();
        newPost.setId(2);
        Integer albumId = 1;
        when(postService.updatePost(albumId, newPost)).thenReturn(flag);
        ResponseEntity<PostDTO> postCon = postController
                .updatePost(albumId, newPost);
        assertNotNull(postCon);
    }

    /**
     * Test of deletePost method, of class PostController.
     */
    @Test
    public void testDeletePost() {
        Integer postId = 1;
        ResponseEntity<PostDTO> postCon = postController.deletePost(postId);
        postService.deletePost(postId);
        assertNotNull(postCon);
    }
    
}
