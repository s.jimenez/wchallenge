/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author jhoan
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "DTO class that stores the company information")
public class CompanyDTO {

    @ApiModelProperty(notes = "name associated with the company")
    private String name;

    @ApiModelProperty(notes = "catch phrase associated with the company")
    private String catchPhrase;

    @ApiModelProperty(notes = "bs associated with the company")
    private String bs;

}
