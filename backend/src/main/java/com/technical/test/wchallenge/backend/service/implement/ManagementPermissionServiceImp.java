/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service.implement;

import com.technical.test.wchallenge.backend.dto.AlbumDTO;
import com.technical.test.wchallenge.backend.dto.UserDTO;
import com.technical.test.wchallenge.backend.dto.UserPermissionDTO;
import com.technical.test.wchallenge.backend.model.ManagementPermissions;
import com.technical.test.wchallenge.backend.model.TypePermissions;
import com.technical.test.wchallenge.backend.repository.ManagementPermissionsRepository;
import com.technical.test.wchallenge.backend.repository.TypePermissionsRepository;
import com.technical.test.wchallenge.backend.service.AlbumService;
import com.technical.test.wchallenge.backend.service.ManagementPermissionService;
import com.technical.test.wchallenge.backend.service.UserService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jhoan
 */
@Service
@Slf4j
public class ManagementPermissionServiceImp implements
        ManagementPermissionService {

    @Autowired
    private ManagementPermissionsRepository managementPermissionsRepository;

    @Autowired
    private TypePermissionsRepository typePermissionsRepository;

    @Autowired
    private AlbumService albumService;

    @Autowired
    private UserService userService;

    @Override
    public ManagementPermissions createPermissions(
            ManagementPermissions permission) {
        log.info("Entry into service function to create a new permission");
        boolean owner = false;
        boolean shared = false;
        ManagementPermissions newPermission;
        AlbumDTO[] albumListByUser = albumService.getAlbumByUser(
                permission.getUserId());
        ArrayList<ManagementPermissions> permissionsListByUser
                = getPermissionsByUser(permission.getUserId());
        for (AlbumDTO album : albumListByUser) {
            if (Objects.equals(album.getId(), permission.getAlbumId())) {
                owner = true;
            }
        }
        for (ManagementPermissions permissionByUser : permissionsListByUser) {
            if (Objects.equals(permissionByUser.getAlbumId(),
                    permission.getAlbumId())) {
                shared = true;
            }
        }
        if (!owner && !shared) {
            newPermission = managementPermissionsRepository.save(permission);
        } else {
            log.info("the user owns the album, does not require "
                    + "permissions or already has the permissions of the "
                    + "album");
            return null;
        }
        return newPermission;
    }

    @Override
    public ManagementPermissions updatePermissionByUser(Integer permissionId,
            ManagementPermissions updatePermission) {
        log.info("Entry into service function to update a permission");
        Optional<ManagementPermissions> permission
                = managementPermissionsRepository.findById(permissionId);
        ManagementPermissions newPermission = new ManagementPermissions();
        newPermission.setAlbumId(permission.get().getAlbumId());
        newPermission.setUserId(permission.get().getUserId());
        newPermission.setId(permissionId);
        newPermission.setTypePermissionId(updatePermission
                .getTypePermissionId());
        managementPermissionsRepository.save(newPermission);
        return newPermission;
    }

    private ArrayList<ManagementPermissions> getPermissionsByUser(
            Integer userId) {
        log.info("Entry into service function to get a permission by user id");
        ArrayList<ManagementPermissions> listPermissionsByUser
                = new ArrayList<>();
        List<ManagementPermissions> listPermissions = getAllPermissions();
        listPermissions.stream().filter((permission) -> (permission.getUserId()
                .equals(userId))).forEachOrdered((permission) -> {
            listPermissionsByUser.add(permission);
        });
        return listPermissionsByUser;
    }

    @Override
    public ArrayList<ManagementPermissions> getAllPermissions() {
        log.info("Entry into service function to get all permissions");
        ArrayList<ManagementPermissions> allPermissions
                = (ArrayList<ManagementPermissions>)
                managementPermissionsRepository.findAll();
        return allPermissions;
    }

    private ArrayList<ManagementPermissions>
            getPermissionsByAlbumAndTypePermission(Integer typePermissionId,
                    Integer albumId) {
        log.info("Entry into service function to get a permission by album id "
                + "and type permission id");
        ArrayList<ManagementPermissions> listPermissionsByAlbumAndTypePermission
                = new ArrayList<>();
        List<ManagementPermissions> listPermissions = getAllPermissions();
        listPermissions.stream().filter((permission) -> (permission.getAlbumId()
                .equals(albumId))).forEachOrdered((permission) -> {
            for (Integer typeId : permission.getTypePermissionId()) {
                if (typeId.equals(typePermissionId)) {
                    listPermissionsByAlbumAndTypePermission.add(
                            permission);
                }
            }
        });
        return listPermissionsByAlbumAndTypePermission;
    }

    @Override
    public ArrayList<UserPermissionDTO> getAllUserByAlbumPermission(
            Integer typePermissionId, Integer albumId) {
        log.info("Entry into service function to get all user associated with "
                + "the album permissions");
        ArrayList<UserPermissionDTO> listUserPermission = new ArrayList<>();
        ArrayList<ManagementPermissions> listPermission
                = getPermissionsByAlbumAndTypePermission(
                        typePermissionId, albumId);
        for (ManagementPermissions permission : listPermission) {
            UserDTO user = userService.getUserById(permission.getUserId());
            UserPermissionDTO userPermission = new UserPermissionDTO();
            userPermission.setTypePermission(
                    getTypePermissionsById(typePermissionId).getType());
            userPermission.setAlbumId(albumId);
            userPermission.setAddress(user.getAddress());
            userPermission.setCompany(user.getCompany());
            userPermission.setEmail(user.getEmail());
            userPermission.setId(user.getId());
            userPermission.setName(user.getName());
            userPermission.setPhone(user.getPhone());
            userPermission.setUsername(user.getUsername());
            userPermission.setWebsite(user.getWebsite());
            listUserPermission.add(userPermission);
        }
        return listUserPermission;
    }

    @Override
    public ArrayList<ManagementPermissions> 
        deletePermission(Integer permissionId) {
        log.info("Entry into service function to delete a permission");
        managementPermissionsRepository.deleteById(permissionId);
        ArrayList<ManagementPermissions> listPermission = getAllPermissions();
        return listPermission;
    }

    @Override
    public TypePermissions createTypePermissions(
            TypePermissions typePermission) {
        log.info("Entry into service function to create a new type permission");
        TypePermissions newTypePermission = new TypePermissions();
        if (typePermission != null) {
            newTypePermission = typePermissionsRepository.save(typePermission);
        }
        return newTypePermission;
    }

    @Override
    public ArrayList<TypePermissions> getAllTypePermissions() {
        log.info("Entry into service function to get all types permissions");
        ArrayList<TypePermissions> allTypes = (ArrayList<TypePermissions>)
                typePermissionsRepository.findAll();
        return allTypes;
    }

    private TypePermissions getTypePermissionsById(Integer permissionId) {
        log.info("Entry into service function to get a type permission by id");
        Optional<TypePermissions> typePermission = typePermissionsRepository
                .findById(permissionId);
        return typePermission.get();
    }
}
