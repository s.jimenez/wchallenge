/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service;

import com.technical.test.wchallenge.backend.dto.AlbumDTO;
import com.technical.test.wchallenge.backend.dto.PhotoDTO;
import com.technical.test.wchallenge.backend.service.implement.PhotoServiceImp;
import com.technical.test.wchallenge.backend.utils.RouteConstants;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author jhoan
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PhotoServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private AlbumService albumService;

    @InjectMocks
    private PhotoServiceImp photoService;   
    
    @Test
    public void testGetAllPhotos() {
        PhotoDTO[] listPhoto = new PhotoDTO[1];
        PhotoDTO newPhoto = new PhotoDTO(1, 1, "nn", "nn", "nn");
        PhotoDTO setPhoto = new PhotoDTO();
        setPhoto.setAlbumId(newPhoto.getAlbumId());
        when(restTemplate.getForEntity(RouteConstants.URL_PHOTO,
                PhotoDTO[].class)).thenReturn(new ResponseEntity<>(
                        listPhoto,HttpStatus.FOUND));
        PhotoDTO[] result = photoService.getAllPhotos();
        assertNotNull(result);
        assertEquals(result.length, listPhoto.length);
    }

    @Test
    public void testCreatePhoto() {
        PhotoDTO newPhoto = new PhotoDTO();
        when(restTemplate.postForEntity(RouteConstants.URL_PHOTO, newPhoto, 
                PhotoDTO.class)).thenReturn(new ResponseEntity<>(newPhoto, 
                        HttpStatus.CREATED));
        PhotoDTO result = photoService.createPhoto(newPhoto);
        assertNotNull(result);
        assertEquals(result, newPhoto);
    }

    @Test
    public void testUpdatePhoto() {
        PhotoDTO updatePhoto = new PhotoDTO(1, 1, "nn", "nn", "nn");
        PhotoDTO setPhoto = new PhotoDTO();
        setPhoto.setAlbumId(updatePhoto.getAlbumId());
        setPhoto.setId(updatePhoto.getId());
        setPhoto.setThumbnailUrl(updatePhoto.getThumbnailUrl());
        setPhoto.setTitle(updatePhoto.getTitle());
        setPhoto.setUrl(updatePhoto.getUrl());
        Integer photoId = 1;
        restTemplate.put(RouteConstants.URL_PHOTO + "/" + photoId, updatePhoto);
        photoService.updatePhoto(photoId, updatePhoto);
        assertNotNull(updatePhoto);
    }

    @Test
    public void testDeletePhoto() {
        PhotoDTO deletePhoto = new PhotoDTO();
        Integer photoId = 1;
        restTemplate.delete(RouteConstants.URL_PHOTO + "/" + photoId,
                deletePhoto);
        photoService.deletePhoto(photoId);
        assertNotNull(deletePhoto);
    }

    @Test
    public void testGetPhotosByUser() {
        Integer userId = 1;
        ArrayList<PhotoDTO> arrayPhoto = new ArrayList<>();
        PhotoDTO[] listPhoto = new PhotoDTO[1];
        PhotoDTO newPhoto = new PhotoDTO(1, 1, "nn", "nn", "nn");
        arrayPhoto.add(newPhoto);
        arrayPhoto.toArray(listPhoto);
        ArrayList<AlbumDTO> arrayAlbum = new ArrayList<>();
        AlbumDTO[] listAlbum = new AlbumDTO[1];
        AlbumDTO newAlbum = new AlbumDTO(1, 1, "nn");
        arrayAlbum.add(newAlbum);
        arrayAlbum.toArray(listAlbum);
        when(albumService.getAlbumByUser(userId)).thenReturn(listAlbum);
        when(restTemplate.getForEntity(RouteConstants.URL_PHOTO_PARAM + userId, 
                PhotoDTO[].class)).thenReturn(new ResponseEntity<>(
                        listPhoto,HttpStatus.FOUND));
        ArrayList<PhotoDTO> result = photoService.getPhotosByUser(userId);
        assertNotNull(result);
        assertEquals(result.size(), arrayPhoto.size());
    }
    
    @Test
    public void testGetPhotosByAlbum() {
        PhotoDTO[] listPhoto = new PhotoDTO[1];
        Integer albumId = 1;
        when(restTemplate.getForEntity(RouteConstants.URL_PHOTO_PARAM + albumId, 
                PhotoDTO[].class)).thenReturn(new ResponseEntity<>(
                        listPhoto,HttpStatus.FOUND));
        PhotoDTO[] result = photoService.getPhotosByAlbum(albumId);
        assertNotNull(result);
        assertEquals(result.length, listPhoto.length);
    }
    
}
