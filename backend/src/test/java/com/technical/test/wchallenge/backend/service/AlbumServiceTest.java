/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service;

import com.technical.test.wchallenge.backend.dto.AlbumDTO;
import com.technical.test.wchallenge.backend.service.implement.AlbumServiceImp;
import com.technical.test.wchallenge.backend.utils.RouteConstants;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author jhoan
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AlbumServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private AlbumServiceImp albumService;
    
    public AlbumServiceTest() {
    }
    
    @Test
    public void testGetAllAlbum() {
        AlbumDTO[] listAlbum = new AlbumDTO[1];
        when(restTemplate.getForEntity(RouteConstants.URL_ALBUM,
                AlbumDTO[].class)).thenReturn(
                new ResponseEntity<>(listAlbum,HttpStatus.FOUND));
        AlbumDTO[] result = albumService.getAllAlbum();
        assertNotNull(result);
        assertEquals(result.length, listAlbum.length);
    }
    
    @Test
    public void testGetAlbumByUserId() {
        AlbumDTO[] listAlbum = new AlbumDTO[1];
        Integer userId = 1;
        when(restTemplate.getForEntity(RouteConstants.URL_ALBUM_PARAM + userId,
                AlbumDTO[].class))
                .thenReturn(new ResponseEntity<>(listAlbum,HttpStatus.FOUND));
        AlbumDTO[] result = albumService.getAlbumByUser(userId);
        assertNotNull(result);
        assertEquals(result.length, listAlbum.length);
    }
    
    @Test
    public void testCreateAlbum() {
        AlbumDTO newAlbum = new AlbumDTO();
        when(restTemplate.postForEntity(RouteConstants.URL_ALBUM, newAlbum,
                AlbumDTO.class))
                .thenReturn(new ResponseEntity<>(newAlbum, HttpStatus.CREATED));
        AlbumDTO result = albumService.createAlbum(newAlbum);
        assertNotNull(result);
        assertEquals(result, newAlbum);
    }
    
    @Test
    public void testUpdateAlbum() {
        AlbumDTO updateAlbum = new AlbumDTO(1, 1, "nn");
        AlbumDTO setAlbum = new AlbumDTO();
        setAlbum.setId(updateAlbum.getId());
        setAlbum.setTitle(updateAlbum.getTitle());
        setAlbum.setUserId(updateAlbum.getUserId());
        Integer albumId = 1;
        restTemplate.put(RouteConstants.URL_ALBUM + "/" + albumId, updateAlbum);
        albumService.updateAlbum(albumId, updateAlbum);
        assertNotNull(updateAlbum);
    }
    
    @Test
    public void testDeleteAlbum() {
        AlbumDTO deleteAlbum = new AlbumDTO();
        Integer albumId = 1;
        restTemplate.delete(RouteConstants.URL_ALBUM + "/" + albumId,
                deleteAlbum);
        albumService.deleteAlbum(albumId);
        assertNotNull(deleteAlbum);
    }
}
