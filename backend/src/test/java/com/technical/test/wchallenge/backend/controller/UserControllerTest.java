/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.controller;

import com.technical.test.wchallenge.backend.dto.UserDTO;
import com.technical.test.wchallenge.backend.service.UserService;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author jhoan
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    @Mock
    private UserService userService;
    
    @InjectMocks
    private UserController userController;

    @Test
    public void testGetAllUsersNotFound() {
        when(userService.getAllUsers()).thenReturn(null);
        ResponseEntity<UserDTO[]> userCon = userController.getAllUsers();
        assertNotNull(userCon);
    }
    
    @Test
    public void testGetAllUsersFound() {
        ArrayList<UserDTO> arrayUser = new ArrayList<>();
        UserDTO[] listUser = new UserDTO[1];
        UserDTO newUser = new UserDTO(1, "sebas", "dark", "sebas@jimenez", null,
                null, null, null);
        arrayUser.add(newUser);
        arrayUser.toArray(listUser);
        when(userService.getAllUsers()).thenReturn(listUser);
        ResponseEntity<UserDTO[]> userCon = userController.getAllUsers();
        assertEquals(userCon.getBody().length, listUser.length);
    }

    @Test
    public void testGetUserByIdNotNull() {
        Integer userId = 1;
        UserDTO newUser = new UserDTO(1, "sebas", "dark", "sebas@jimenez", null,
                null, null, null);
        when(userService.getUserById(userId)).thenReturn(newUser);
        ResponseEntity<UserDTO> userCon = userController.getUserById(userId);
        assertEquals(userCon.getBody(), newUser);
    }
    
    @Test
    public void testGetUserByIdNull() {
        Integer userId = 1;
        when(userService.getUserById(userId)).thenReturn(null);
        ResponseEntity<UserDTO> userCon = userController.getUserById(userId);
        assertEquals(userCon.getBody(), null);
    }

    @Test
    public void testCreateUserNull() {
        UserDTO newUser = new UserDTO();
        when(userService.createUser(newUser)).thenReturn(null);
        ResponseEntity<UserDTO> userCon = userController.createUser(newUser);
        assertNull(userCon.getBody());
    }
    
    @Test
    public void testCreateUserNotNull() {
        UserDTO newUser = new UserDTO(1, "sebas", "dark", "sebas@jimenez", null,
                null, null, null);
        when(userService.createUser(newUser)).thenReturn(newUser);
        ResponseEntity<UserDTO> userCon = userController.createUser(newUser);
        assertEquals(userCon.getBody(), newUser);
    }

    @Test
    public void testUpdateUserOk() {
        boolean flag = true;
        UserDTO newUser = new UserDTO();
        newUser.setId(1);
        Integer userId = 1;
        when(userService.updateUser(userId, newUser)).thenReturn(flag);
        ResponseEntity<UserDTO> userCon = userController
                .updateUser(userId, newUser);
        assertNotNull(userCon);
        assertTrue(flag);
    }

    @Test
    public void testUpdateUserBadRequest() {
        boolean flag = false;
        UserDTO newUser = new UserDTO();
        newUser.setId(1);
        Integer userId = 1;
        when(userService.updateUser(userId, newUser)).thenReturn(flag);
        ResponseEntity<UserDTO> userCon = userController
                .updateUser(userId, newUser);
        assertNotNull(userCon);
    }

    @Test
    public void testDeleteUser() {
        Integer userId = 1;
        ResponseEntity<UserDTO> userCon = userController.deleteUser(userId);
        userService.deleteUser(userId);
        assertNotNull(userCon);
    }
    
}
