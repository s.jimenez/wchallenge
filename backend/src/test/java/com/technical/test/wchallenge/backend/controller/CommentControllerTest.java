/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.controller;

import com.technical.test.wchallenge.backend.dto.CommentDTO;
import com.technical.test.wchallenge.backend.service.CommentService;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author jhoan
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CommentControllerTest {

    @Mock
    private CommentService commentService;
    
    @InjectMocks
    private CommentController commentController;
    
    @Test
    public void testGetAllCommentsNotFound() {
        when(commentService.getAllComments()).thenReturn(null);
        ResponseEntity<CommentDTO[]> commentCon = commentController
                .getAllComments();
        assertNotNull(commentCon);
    }
    
    @Test
    public void testGetAllCommentsFound() {
        ArrayList<CommentDTO> arrayComment = new ArrayList<>();
        CommentDTO[] listComment = new CommentDTO[1];
        CommentDTO newComment = new CommentDTO(1, 1, "sebas", "dark",
                "sebas@jimenez");
        arrayComment.add(newComment);
        arrayComment.toArray(listComment);
        when(commentService.getAllComments()).thenReturn(listComment);
        ResponseEntity<CommentDTO[]> commentCon = commentController
                .getAllComments();
        assertEquals(commentCon.getBody().length, listComment.length);
    }

    @Test
    public void testGetCommentsByUserNotFound() {
        Integer userId = 1;
        when(commentService.getCommentsByUser(userId)).thenReturn(null);
        ResponseEntity<ArrayList<CommentDTO>> commentCon = commentController
                .getCommentsByUser(userId);
        assertNotNull(commentCon);
    }
    
    @Test
    public void testGetCommentsByUserFound() {
        Integer userId = 1;
        ArrayList<CommentDTO> arrayComment = new ArrayList<>();
        CommentDTO newComment = new CommentDTO(1, 1, "sebas", "dark",
                "sebas@jimenez");
        arrayComment.add(newComment);
        when(commentService.getCommentsByUser(userId)).thenReturn(arrayComment);
        ResponseEntity<ArrayList<CommentDTO>> commentCon = commentController
                .getCommentsByUser(userId);
        assertNotNull(commentCon);
    }

    @Test
    public void testGetCommentsByNameNotFound() {
        String name = "sebas";
        when(commentService.getCommentsByName(name)).thenReturn(null);
        ResponseEntity<ArrayList<CommentDTO>> commentCon = commentController
                .getCommentsByName(name);
        assertNotNull(commentCon);
    }
    
    @Test
    public void testGetCommentsByNameFound() {
        String name = "sebas";
        ArrayList<CommentDTO> arrayComment = new ArrayList<>();
        CommentDTO newComment = new CommentDTO(1, 1, "sebas", "dark",
                "sebas@jimenez");
        arrayComment.add(newComment);
        when(commentService.getCommentsByName(name)).thenReturn(arrayComment);
        ResponseEntity<ArrayList<CommentDTO>> commentCon = commentController
                .getCommentsByName(name);
        assertNotNull(commentCon);
    }

    @Test
    public void testCreateCommentNull() {
        CommentDTO newComment = new CommentDTO();
        when(commentService.createComment(newComment)).thenReturn(null);
        ResponseEntity<CommentDTO> commentCon = commentController
                .createComment(newComment);
        assertNull(commentCon.getBody());
    }
    
    @Test
    public void testCreateCommentNotNull() {
        CommentDTO newComment = new CommentDTO(1, 1, "sebas", "dark",
                "sebas@jimenez");
        when(commentService.createComment(newComment)).thenReturn(newComment);
        ResponseEntity<CommentDTO> commentCon = commentController
                .createComment(newComment);
        assertEquals(commentCon.getBody(), newComment);
    }

    @Test
    public void testUpdateCommentOk() {
        boolean flag = true;
        Integer commentId = 1;
        CommentDTO newComment = new CommentDTO();
        newComment.setId(1);
        when(commentService.updateComment(commentId, newComment))
                .thenReturn(flag);
        ResponseEntity<CommentDTO> commentCon = commentController
                .updateComment(commentId, newComment);
        assertNotNull(commentCon);
        assertTrue(flag);
    }

    @Test
    public void testUpdateCommentBadRequest() {
        boolean flag = false;
        Integer commentId = 1;
        CommentDTO newComment = new CommentDTO();
        newComment.setId(2);
        when(commentService.updateComment(commentId, newComment))
                .thenReturn(flag);
        ResponseEntity<CommentDTO> commentCon = commentController
                .updateComment(commentId, newComment);
        assertNotNull(commentCon);
    }

    @Test
    public void testDeleteComment() {
        Integer commentId = 1;
        ResponseEntity<CommentDTO> commentCon = commentController
                .deleteComment(commentId);
        commentService.deleteComment(commentId);
        assertNotNull(commentCon);
    }
    
}
