/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.controller;

import com.technical.test.wchallenge.backend.dto.PhotoDTO;
import com.technical.test.wchallenge.backend.service.PhotoService;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author jhoan
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PhotoControllerTest {

    @Mock
    private PhotoService photoService;
    
    @InjectMocks
    private PhotoController photoController;

    @Test
    public void testGetAllPhotosNotFound() {
        when(photoService.getAllPhotos()).thenReturn(null);
        ResponseEntity<PhotoDTO[]> photoCon = photoController.getAllPhotos();
        assertNotNull(photoCon);
    }
    
    @Test
    public void testGetAllPhotosFound() {
        ArrayList<PhotoDTO> arrayPhoto = new ArrayList<>();
        PhotoDTO[] listPhoto = new PhotoDTO[1];
        PhotoDTO newPhoto = new PhotoDTO(1, 1, "nn", "nn", "nn");
        arrayPhoto.add(newPhoto);
        arrayPhoto.toArray(listPhoto);
        when(photoService.getAllPhotos()).thenReturn(listPhoto);
        ResponseEntity<PhotoDTO[]> photoCon = photoController.getAllPhotos();
        assertEquals(photoCon.getBody().length, listPhoto.length);
    }

    @Test
    public void testGetPhotosByUserNotNull() {
        Integer userId = 1;
        ArrayList<PhotoDTO> arrayPhoto = new ArrayList<>();
        PhotoDTO newPhoto = new PhotoDTO(1, 1, "nn", "nn", "nn");
        arrayPhoto.add(newPhoto);
        when(photoService.getPhotosByUser(userId)).thenReturn(arrayPhoto);
        ResponseEntity<ArrayList<PhotoDTO>> photoCon = photoController.
                getPhotosByUser(userId);
        assertEquals(photoCon.getBody(), arrayPhoto);
    }
    
    @Test
    public void testGetPhotosByUserNull() {
        Integer userId = 1;
        when(photoService.getPhotosByUser(userId)).thenReturn(null);
        ResponseEntity<ArrayList<PhotoDTO>> photoCon = photoController
                .getPhotosByUser(userId);
        assertEquals(photoCon.getBody(), null);
    }

    @Test
    public void testCreatePhotoNull() {
        PhotoDTO newPhoto = new PhotoDTO();
        when(photoService.createPhoto(newPhoto)).thenReturn(null);
        ResponseEntity<PhotoDTO> photoCon = photoController.createPhoto(
                newPhoto);
        assertNull(photoCon.getBody());
    }
    
    @Test
    public void testCreatePhotoNotNull() {
        PhotoDTO newPhoto = new PhotoDTO(1, 1, "nn", "nn", "nn");
        when(photoService.createPhoto(newPhoto)).thenReturn(newPhoto);
        ResponseEntity<PhotoDTO> photoCon = photoController.createPhoto(
                newPhoto);
        assertEquals(photoCon.getBody(), newPhoto);
    }

    @Test
    public void testUpdatePhotoOk() {
        boolean flag = true;
        Integer photoId = 1;
        PhotoDTO newPhoto = new PhotoDTO();
        newPhoto.setId(1);
        when(photoService.updatePhoto(photoId, newPhoto)).thenReturn(flag);
        ResponseEntity<PhotoDTO> photoCon = photoController
                .updatePhoto(photoId, newPhoto);
        assertNotNull(photoCon);
        assertTrue(flag);
    }
    
    @Test
    public void testUpdatePhotoBadRequest() {
        boolean flag = false;
        Integer photoId = 1;
        PhotoDTO newPhoto = new PhotoDTO();
        newPhoto.setId(2);
        when(photoService.updatePhoto(photoId, newPhoto)).thenReturn(flag);
        ResponseEntity<PhotoDTO> photoCon = photoController
                .updatePhoto(photoId, newPhoto);
        assertNotNull(photoCon);
    }

    @Test
    public void testDeletePhoto() {
        Integer photoId = 1;
        ResponseEntity<PhotoDTO> photoCon = photoController.deletePhoto(photoId);
        photoService.deletePhoto(photoId);
        assertNotNull(photoCon);
    }
    
}
