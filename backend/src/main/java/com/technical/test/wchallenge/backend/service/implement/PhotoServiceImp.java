/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service.implement;

import com.technical.test.wchallenge.backend.dto.AlbumDTO;
import com.technical.test.wchallenge.backend.dto.PhotoDTO;
import com.technical.test.wchallenge.backend.service.AlbumService;
import com.technical.test.wchallenge.backend.service.PhotoService;
import com.technical.test.wchallenge.backend.utils.RouteConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author jhoan
 */
@Service
@Slf4j
public class PhotoServiceImp implements PhotoService {

    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private AlbumService albumService;
    
    @Override
    public ArrayList<PhotoDTO> getPhotosByUser(Integer userId) {
        log.info("Entry into service function to get a photo by user id");
        ArrayList<PhotoDTO> photosByUser = new ArrayList<>();
        AlbumDTO[] albumByUser = albumService.getAlbumByUser(userId);
        for (AlbumDTO album : albumByUser) {
            PhotoDTO[] photoByUser = getPhotosByAlbum(album.getId());
            photosByUser.addAll(Arrays.asList(photoByUser));
        }
        return photosByUser;
    }
    
    public PhotoDTO[] getPhotosByAlbum(Integer albumId) {
        log.info("Entry into service function to get a photo by album id");
        ResponseEntity<PhotoDTO[]> response = restTemplate
                .getForEntity(RouteConstants.URL_PHOTO_PARAM + albumId,
                        PhotoDTO[].class);
        return response.getBody();
    }
    
    @Override
    public PhotoDTO[] getAllPhotos() {
        log.info("Entry into service function to get all photos");
        ResponseEntity<PhotoDTO[]> response = restTemplate
                .getForEntity(RouteConstants.URL_PHOTO, PhotoDTO[].class);
        return response.getBody();
    }

    @Override
    public PhotoDTO createPhoto(PhotoDTO newPhoto) {
        log.info("Entry into service function to create a new photo");
        ResponseEntity<PhotoDTO> response = restTemplate
                .postForEntity(RouteConstants.URL_PHOTO, newPhoto,
                        PhotoDTO.class);
        return response.getBody();
    }

    @Override
    public Boolean updatePhoto(Integer photoId, PhotoDTO newPhoto) {
        log.info("Entry into service function to update a photo");
        boolean flag = false;
        String updateURL = RouteConstants.URL_PHOTO + "/" + newPhoto.getId();
        if (Objects.equals(photoId, newPhoto.getId())) {
            restTemplate.put(updateURL, newPhoto);
            flag = true;
        }
        return flag;
    }

    @Override
    public void deletePhoto(Integer photoId) {
        log.info("Entry into service function to delete a photo by id");
        String deleteURL = RouteConstants.URL_PHOTO + "/" + photoId;
        restTemplate.delete(deleteURL);
    }

}
