/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service.implement;

import com.technical.test.wchallenge.backend.dto.AlbumDTO;
import com.technical.test.wchallenge.backend.service.AlbumService;
import com.technical.test.wchallenge.backend.utils.RouteConstants;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author jhoan
 */
@Service
@Slf4j
public class AlbumServiceImp implements AlbumService{
    
    @Autowired
    private RestTemplate restTemplate;
    
    @Override
    public AlbumDTO[] getAllAlbum() {
        log.info("Entry into service function to get all albums");
        ResponseEntity<AlbumDTO[]> response = restTemplate
                .getForEntity(RouteConstants.URL_ALBUM, AlbumDTO[].class);
        return response.getBody();
    }
    
    @Override
    public AlbumDTO[] getAlbumByUser(Integer userId) {
        log.info("Entry into service function to get all albums by user id");
        ResponseEntity<AlbumDTO[]> response = restTemplate
                .getForEntity(RouteConstants.URL_ALBUM_PARAM + userId,
                        AlbumDTO[].class);
        return response.getBody();
    }
    
    @Override
    public AlbumDTO createAlbum(AlbumDTO newAlbum) {
        log.info("Entry into service function to create a new album");
        ResponseEntity<AlbumDTO> response = restTemplate
                .postForEntity(RouteConstants.URL_ALBUM, newAlbum,
                        AlbumDTO.class);
        return response.getBody();
    }
    
    @Override
    public boolean updateAlbum(Integer albumId, AlbumDTO newAlbum) {
        log.info("Entry into service function to update an album");
        boolean flag = false;
        String updateURL = RouteConstants.URL_ALBUM + "/" + albumId;
        if (Objects.equals(albumId, newAlbum.getId())) {
            restTemplate.put(updateURL, newAlbum);
            flag = true;
        }
        return flag;
    }
    
    @Override
    public void deleteAlbum(Integer albumId) {
        log.info("Entry into service function to delete an album by id");
        String deleteURL = RouteConstants.URL_ALBUM + "/" + albumId;
        restTemplate.delete(deleteURL);
    }
}
