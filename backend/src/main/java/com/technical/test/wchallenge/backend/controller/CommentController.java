/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.controller;

import com.technical.test.wchallenge.backend.dto.CommentDTO;
import com.technical.test.wchallenge.backend.service.CommentService;
import com.technical.test.wchallenge.backend.utils.HttpConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@RequestMapping(value = "/api/v1/comments")
@Api(value = "Management of comment resources",
        description = "Receive all comment information from an external api")
@Slf4j
public class CommentController {

    @Autowired
    private CommentService commentService;

    @ApiOperation(value = "Get all comment information from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Comment information found",
                response = CommentDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_NOT_FOUND,
                message = "The information could not be found")
    })
    @GetMapping
    public ResponseEntity<CommentDTO[]> getAllComments() {
        log.info("Connecting api with controller to get all comments");
        CommentDTO[] response = commentService.getAllComments();
        if (response == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Get all comment information associated with the "
            + "user id from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Comment information found",
                response = CommentDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_NOT_FOUND,
                message = "The information could not be found")
    })
    @GetMapping("/byUser/{userId}")
    public ResponseEntity<ArrayList<CommentDTO>> getCommentsByUser(
            @PathVariable Integer userId) {
        log.info("Connecting api with controller to get all comments by user "
                + "id");
        ArrayList<CommentDTO> response = commentService
                .getCommentsByUser(userId);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Get all comment information associated with the "
            + "name from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Comment information found",
                response = CommentDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_NOT_FOUND,
                message = "The information could not be found")
    })
    @GetMapping("/by/{name}")
    public ResponseEntity<ArrayList<CommentDTO>> getCommentsByName(
            @PathVariable final String name) {
        log.info("Connecting api with controller to get all comments associated"
                + " with the name");
        ArrayList<CommentDTO> response = commentService.getCommentsByName(name);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Create a new comment information from an external "
            + "api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_CREATED,
                message = "The commnet has been created correctly",
                response = CommentDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_BAD_REQUEST,
                message = "The action could not be performed")
    })
    @PostMapping
    public ResponseEntity<CommentDTO> createComment(
            @RequestBody final CommentDTO newComment) {
        log.info("Connecting api with controller to create a new comment");
        CommentDTO response = commentService.createComment(newComment);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update a comment information from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "The comment has been updated correctly",
                response = CommentDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_BAD_REQUEST,
                message = "The action could not be performed")
    })
    @PutMapping("{commentId}")
    public ResponseEntity<CommentDTO> updateComment(
            @PathVariable final Integer commentId,
            @RequestBody final CommentDTO newComment) {
        log.info("Connecting api with controller to update a comment");
        Boolean response = commentService.updateComment(commentId, newComment);
        if (response == false) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Delete a comment information from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "The comment has been deleted correctly",
                response = CommentDTO.class)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<CommentDTO> deleteComment(
            @PathVariable final Integer id) {
        log.info("Connecting api with controller to delete a comment by id");
        commentService.deleteComment(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
