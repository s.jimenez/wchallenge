/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service;

import com.technical.test.wchallenge.backend.dto.CommentDTO;
import java.util.ArrayList;

/**
 *
 * @author jhoan
 */
public interface CommentService {
    
    CommentDTO[] getAllComments();

    CommentDTO createComment(CommentDTO newComment);

    Boolean updateComment(Integer commentId, CommentDTO newComment);

    void deleteComment(Integer commentId);
    
    CommentDTO[] getCommentsByPost(Integer postId);
    
    ArrayList<CommentDTO> getCommentsByUser(Integer userId);
    
    ArrayList<CommentDTO> getCommentsByName(String name);

}
