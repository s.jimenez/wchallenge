/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.controller;

import com.technical.test.wchallenge.backend.dto.PostDTO;
import com.technical.test.wchallenge.backend.service.PostService;
import com.technical.test.wchallenge.backend.utils.HttpConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@RequestMapping(value = "/api/v1/posts")
@Api(value = "Management of post resources",
        description = "Receive all post information from an external api")
@Slf4j
public class PostController {

    @Autowired
    private PostService postService;

    @ApiOperation(value = "Get all post information from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Post information found", response = PostDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_NOT_FOUND,
                message = "The information could not be found")
    })
    @GetMapping
    public ResponseEntity<PostDTO[]> getAllPosts() {
        log.info("Connecting api with controller to get all posts");
        PostDTO[] response = postService.getAllPost();
        if (response == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Create a new post information from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_CREATED,
                message = "Post information has been created correctly",
                response = PostDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_BAD_REQUEST,
                message = "The action could not be performed")
    })
    @PostMapping
    public ResponseEntity<PostDTO> createPost(
            @RequestBody final PostDTO newPost) {
        log.info("Connecting api with controller to create a new post");
        PostDTO response = postService.createPost(newPost);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update a post information from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK, 
                message = "Post information has been updated correctly",
                response = PostDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_BAD_REQUEST, 
                message = "The action could not be performed")
    })
    @PutMapping("/{postId}")
    public ResponseEntity<PostDTO> updatePost(
            @PathVariable final Integer postId,
            @RequestBody final PostDTO newPost) {
        log.info("Connecting api with controller to update a post");
        Boolean response = postService.updatePost(postId, newPost);
        if (response == false) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Delete a post information associated with the id "
            + "from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK, 
                message = "Post information has been deleted correctly",
                response = PostDTO.class)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<PostDTO> deletePost(
            @PathVariable final Integer id) {
        log.info("Connecting api with controller to delete a post by id");
        postService.deletePost(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
