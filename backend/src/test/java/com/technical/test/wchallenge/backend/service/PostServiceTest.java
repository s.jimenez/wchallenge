/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service;

import com.technical.test.wchallenge.backend.dto.PostDTO;
import com.technical.test.wchallenge.backend.service.implement.PostServiceImp;
import com.technical.test.wchallenge.backend.utils.RouteConstants;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author jhoan
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PostServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private PostServiceImp postService;  

    @Test
    public void testGetAllPost() {
        PostDTO[] listPost = new PostDTO[1];
        when(restTemplate.getForEntity(RouteConstants.URL_POST,
                PostDTO[].class))
                .thenReturn(new ResponseEntity<>(listPost,HttpStatus.FOUND));
        PostDTO[] result = postService.getAllPost();
        assertNotNull(result);
        assertEquals(result.length, listPost.length);
    }

    @Test
    public void testCreatePost() {
        PostDTO newPost = new PostDTO();
        when(restTemplate.postForEntity(RouteConstants.URL_POST, newPost,
                PostDTO.class))
                .thenReturn(new ResponseEntity<>(newPost, HttpStatus.CREATED));
        PostDTO result = postService.createPost(newPost);
        assertNotNull(result);
        assertEquals(result, newPost);
    }

    @Test
    public void testUpdatePost() {
        PostDTO updatePost = new PostDTO(1, 1, "nn", "nn");
        Integer postId = 1;
        restTemplate.put(RouteConstants.URL_POST + "/" + postId, updatePost);
        postService.updatePost(postId, updatePost);
        assertNotNull(updatePost);
    }

    @Test
    public void testDeletePost() {
        PostDTO deletePost = new PostDTO(1, 1, "nn", "nn");
        PostDTO setPost = new PostDTO();
        setPost.setBody(deletePost.getBody());
        setPost.setId(deletePost.getId());
        setPost.setTitle(deletePost.getTitle());
        setPost.setUserId(deletePost.getUserId());
        Integer postId = 1;
        restTemplate.delete(RouteConstants.URL_POST + "/" + postId, deletePost);
        postService.deletePost(1);
        assertNotNull(deletePost);
    }

    @Test
    public void testGetPostByUser() {
        PostDTO[] listPost = new PostDTO[1];
        Integer userId = 1;
        when(restTemplate.getForEntity(RouteConstants.URL_POST_PARAM + userId,
                PostDTO[].class))
                .thenReturn(new ResponseEntity<>(listPost,HttpStatus.FOUND));
        PostDTO[] result = postService.getPostByUser(userId);
        assertNotNull(result);
        assertEquals(result.length, listPost.length);
    }
    
}
