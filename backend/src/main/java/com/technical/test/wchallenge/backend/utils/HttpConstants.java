/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.utils;

/**
 *
 * @author jhoan
 */
public final class HttpConstants {

    //Http status for all controller
    public static final int HTTP_STATUS_OK = 200;
    
    public static final int HTTP_STATUS_CREATED = 201;
    
    public static final int HTTP_STATUS_BAD_REQUEST = 400;
    
    public static final int HTTP_STATUS_NOT_FOUND = 404;

}
