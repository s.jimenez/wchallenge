/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service;

import com.technical.test.wchallenge.backend.dto.PostDTO;

/**
 *
 * @author jhoan
 */
public interface PostService {

    PostDTO[] getAllPost();

    PostDTO createPost(PostDTO newPost);

    Boolean updatePost(Integer postId, PostDTO newPost);

    void deletePost(Integer postId);
    
    PostDTO[] getPostByUser(Integer userId);

}
