/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.controller;

import com.technical.test.wchallenge.backend.dto.AlbumDTO;
import com.technical.test.wchallenge.backend.service.AlbumService;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author jhoan
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AlbumControllerTest {

    @Mock
    private AlbumService albumService;
    
    @InjectMocks
    private AlbumController albumController;

    @Test
    public void testGetAllAlbumNotFound() {
    when(albumService.getAllAlbum()).thenReturn(null);
        ResponseEntity<AlbumDTO[]> albumCon = albumController.getAllAlbum();
        assertNotNull(albumCon);
    }
    
    @Test
    public void testGetAllAlbumFound() {
        ArrayList<AlbumDTO> arrayAlbum = new ArrayList<>();
        AlbumDTO[] listAlbum = new AlbumDTO[1];
        AlbumDTO newAlbum = new AlbumDTO(1, 1, "sebas");
        arrayAlbum.add(newAlbum);
        arrayAlbum.toArray(listAlbum);
        when(albumService.getAllAlbum()).thenReturn(listAlbum);
        ResponseEntity<AlbumDTO[]> albumCon = albumController.getAllAlbum();
        assertEquals(albumCon.getBody().length, listAlbum.length);
    }

    @Test
    public void testGetAlbumByUserNotNull() {
        Integer userId = 1;
        ArrayList<AlbumDTO> arrayAlbum = new ArrayList<>();
        AlbumDTO[] listAlbum = new AlbumDTO[1];
        AlbumDTO newAlbum = new AlbumDTO(1, 1, "sebas");
        arrayAlbum.add(newAlbum);
        arrayAlbum.toArray(listAlbum);
        when(albumService.getAlbumByUser(userId)).thenReturn(listAlbum);
        ResponseEntity<AlbumDTO[]> albumCon = albumController.
                getAlbumByUser(userId);
        assertEquals(albumCon.getBody().length, listAlbum.length);
    }
    
    @Test
    public void testGetAlbumByUserNull() {
        Integer userId = 1;
        when(albumService.getAlbumByUser(userId)).thenReturn(null);
        ResponseEntity<AlbumDTO[]> albumCon = albumController
                .getAlbumByUser(userId);
        assertNull(albumCon.getBody());
    }

    @Test
    public void testCreateAlbumNull() {
        AlbumDTO newAlbum = new AlbumDTO();
        when(albumService.createAlbum(newAlbum)).thenReturn(null);
        ResponseEntity<AlbumDTO> albumCon = albumController
                .createAlbum(newAlbum);
        assertNull(albumCon.getBody());
    }
    
    @Test
    public void testCreateAlbumNotNull() {
        AlbumDTO newAlbum = new AlbumDTO(1, 1, "sebas");
        when(albumService.createAlbum(newAlbum)).thenReturn(newAlbum);
        ResponseEntity<AlbumDTO> albumCon = albumController
                .createAlbum(newAlbum);
        assertEquals(albumCon.getBody(), newAlbum);
    }

    @Test
    public void testUpdateAlbumOk() {
        boolean flag = true;
        Integer albumId = 1;
        AlbumDTO newAlbum = new AlbumDTO();
        newAlbum.setId(1);
        when(albumService.updateAlbum(albumId, newAlbum)).thenReturn(flag);
        ResponseEntity<Boolean> albumCon = albumController
                .updateAlbum(albumId, newAlbum);
        assertNotNull(albumCon);
        assertTrue(albumCon.getBody());
    }
    
    @Test
    public void testUpdateAlbumBadRequest() {
        boolean flag = false;
        Integer albumId = 1;
        AlbumDTO newAlbum = new AlbumDTO();
        newAlbum.setId(2);
        when(albumService.updateAlbum(albumId, newAlbum)).thenReturn(flag);
        ResponseEntity<Boolean> albumCon = albumController
                .updateAlbum(albumId, newAlbum);
        assertNotNull(albumCon);
    }

    @Test
    public void testDeleteAlbum() {
        Integer albumId = 1;
        ResponseEntity<AlbumDTO> albumCon = albumController
                .deleteAlbum(albumId);
        albumService.deleteAlbum(albumId);
        assertNotNull(albumCon);
    }
    
}
