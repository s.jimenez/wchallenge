/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service;

import com.technical.test.wchallenge.backend.dto.CommentDTO;
import com.technical.test.wchallenge.backend.dto.PostDTO;
import com.technical.test.wchallenge.backend.service.implement.CommentServiceImp;
import com.technical.test.wchallenge.backend.utils.RouteConstants;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author jhoan
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CommentServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private PostService postService;

    @InjectMocks
    private CommentServiceImp commentService;

    @Test
    public void testGetAllComments() {
        CommentDTO[] listComment = new CommentDTO[1];
        when(restTemplate.getForEntity(RouteConstants.URL_COMMENT,
                CommentDTO[].class))
                .thenReturn(new ResponseEntity<>(listComment,HttpStatus.FOUND));
        CommentDTO[] result = commentService.getAllComments();
        assertNotNull(result);
        assertEquals(result.length, listComment.length);
    }

    @Test
    public void testCreateComment() {
        CommentDTO newComment = new CommentDTO();
        when(restTemplate.postForEntity(RouteConstants.URL_COMMENT, newComment,
                CommentDTO.class)).thenReturn(new ResponseEntity<>(newComment, 
                        HttpStatus.CREATED));
        CommentDTO result = commentService.createComment(newComment);
        assertNotNull(result);
        assertEquals(result, newComment);
    }

    @Test
    public void testUpdateComment() {
        CommentDTO updateComment = new CommentDTO(1, 1, "nn", "nn", "nn");
        CommentDTO setComment = new CommentDTO();
        setComment.setBody(updateComment.getBody());
        setComment.setEmail(updateComment.getEmail());
        setComment.setId(updateComment.getId());
        setComment.setName(updateComment.getName());
        setComment.setPostId(updateComment.getPostId());
        Integer commentId = 1;
        restTemplate.put(RouteConstants.URL_COMMENT + "/" + commentId,
                updateComment);
        commentService.updateComment(commentId, updateComment);
        assertNotNull(updateComment);
    }

    @Test
    public void testDeleteComment() {
        CommentDTO deleteComment = new CommentDTO();
        Integer commentId = 1;
        restTemplate.delete(RouteConstants.URL_COMMENT + "/" + commentId,
                deleteComment);
        commentService.deleteComment(1);
        assertNotNull(deleteComment);
    }

    @Test
    public void testGetCommentsByPost() {
        CommentDTO[] listComment = new CommentDTO[1];
        Integer userId = 1;
        when(restTemplate.getForEntity(RouteConstants.URL_COMMENT_PARAM +
                userId, CommentDTO[].class)).thenReturn(
                        new ResponseEntity<>(listComment,HttpStatus.FOUND));
        CommentDTO[] result = commentService.getCommentsByPost(userId);
        assertNotNull(result);
        assertEquals(result.length, listComment.length);
    }

    @Test
    public void testGetCommentsByUser() {
        Integer userId = 1;
        CommentDTO[] listComment = new CommentDTO[1];
        ArrayList<CommentDTO> arrayComment = new ArrayList<>();
        CommentDTO newComment = new CommentDTO(1, 1, "nn", "nn", "nn");
        arrayComment.add(newComment);
        arrayComment.toArray(listComment);
        PostDTO[] listPost = new PostDTO[1];
        ArrayList<PostDTO> arrayPost = new ArrayList<>();
        PostDTO newPost = new PostDTO(1, 1, "nn", "nn");
        arrayPost.add(newPost);
        arrayPost.toArray(listPost);
        when(postService.getPostByUser(userId)).thenReturn(listPost);
        when(restTemplate.getForEntity(RouteConstants.URL_COMMENT_PARAM +
                userId, CommentDTO[].class)).thenReturn(
                        new ResponseEntity<>(listComment,HttpStatus.FOUND));
        ArrayList<CommentDTO> result = commentService.getCommentsByUser(userId);
        assertNotNull(result);
    }

    @Test
    public void testGetCommentsByName() {
        CommentDTO[] listComment = new CommentDTO[1];
        ArrayList<CommentDTO> arrayComment = new ArrayList<>();
        CommentDTO newComment = new CommentDTO(1, 1, "sebas", "nn", "nn");
        arrayComment.add(newComment);
        arrayComment.toArray(listComment);
        String user = "sebas";
        when(restTemplate.getForEntity(RouteConstants.URL_COMMENT,
                CommentDTO[].class))
                .thenReturn(new ResponseEntity<>(listComment,HttpStatus.FOUND));
        ArrayList<CommentDTO> result = commentService.getCommentsByName(user);
        assertNotNull(result);
    }
    
}
