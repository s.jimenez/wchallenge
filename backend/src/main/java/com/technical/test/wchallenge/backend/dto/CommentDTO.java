/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author jhoan
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "DTO class that stores the comment information")
public class CommentDTO {

    @ApiModelProperty(notes = "post id associated with the comment")
    private Integer postId;

    @ApiModelProperty(notes = "id associated with the comment")
    private Integer id;

    @ApiModelProperty(notes = "name associated with the comment")
    private String name;

    @ApiModelProperty(notes = "email associated with the comment")
    private String email;

    @ApiModelProperty(notes = "body associated with the comment")
    private String body;

}
