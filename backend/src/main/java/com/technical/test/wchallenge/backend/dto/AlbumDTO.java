/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author jhoan
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "DTO class that stores the album information")
public class AlbumDTO {

    @ApiModelProperty(notes = "user id associated with the album")
    private Integer userId;

    @ApiModelProperty(notes = "id associated with the album")
    private Integer id;

    @ApiModelProperty(notes = "title associated with the album")
    private String title;

}
