/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author jhoan
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "DTO class that stores the user information")
public class UserDTO {

    @ApiModelProperty(notes = "id associated with the user")
    private Integer id;

    @ApiModelProperty(notes = "name associated with the user")
    private String name;

    @ApiModelProperty(notes = "username associated with the user")
    private String username;

    @ApiModelProperty(notes = "email associated with the user")
    private String email;

    @ApiModelProperty(notes = "DTO class address associated with the user")
    private AddressDTO address;

    @ApiModelProperty(notes = "phone associated with the user")
    private String phone;

    @ApiModelProperty(notes = "website associated with the user")
    private String website;

    @ApiModelProperty(notes = "DTO class company associated with the user")
    private CompanyDTO company;

}
