/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service;

import com.technical.test.wchallenge.backend.dto.PhotoDTO;
import java.util.ArrayList;

/**
 *
 * @author jhoan
 */
public interface PhotoService {
   
    PhotoDTO[] getAllPhotos();

    PhotoDTO createPhoto(PhotoDTO newPhotos);

    Boolean updatePhoto(Integer photoId, PhotoDTO newPhoto);

    void deletePhoto(Integer photoId);

    ArrayList<PhotoDTO> getPhotosByUser(Integer userId);
}
