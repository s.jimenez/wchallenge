/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author jhoan
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "permissions")
@ApiModel(description = "Model class that stores the management permission "
        + "information")
public class ManagementPermissions implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "id associated with the management permission")
    private Integer id;

    @ApiModelProperty(notes = "user id associated with the management "
            + "permission")
    private Integer userId;

    @ApiModelProperty(notes = "album id associated with the management "
            + "permission")
    private Integer albumId;

    @ApiModelProperty(notes = "list integers associated with the management "
            + "permission")
    private Integer[] typePermissionId;

}
