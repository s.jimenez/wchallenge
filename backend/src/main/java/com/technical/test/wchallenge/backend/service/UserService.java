/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service;

import com.technical.test.wchallenge.backend.dto.UserDTO;


/**
 *
 * @author jhoan
 */
public interface UserService {

    UserDTO[] getAllUsers();

    UserDTO createUser(UserDTO newPost);

    Boolean updateUser(Integer userId, UserDTO newPost);

    void deleteUser(Integer userId);

    UserDTO getUserById(Integer userId);

}
