/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.controller;

import com.technical.test.wchallenge.backend.dto.AlbumDTO;
import com.technical.test.wchallenge.backend.service.AlbumService;
import com.technical.test.wchallenge.backend.utils.HttpConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan    
 */
@RestController
@RequestMapping(value = "/api/v1/albums")
@Api(value = "Management of album resources",
        description = "Receive all album information from an external api")
@Slf4j
public class AlbumController {

    @Autowired
    private AlbumService albumService;

    @ApiOperation(value = "Get all album information from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Album information found", response = AlbumDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_NOT_FOUND,
                message = "The information could not be found")
    })
    @GetMapping
    public ResponseEntity<AlbumDTO[]> getAllAlbum() {
        log.info("Connecting api with controller to get all albums");
        AlbumDTO[] response = albumService.getAllAlbum();
        if (response == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Get all album information associated to user id "
            + "from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Album information found", response = AlbumDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_NOT_FOUND,
                message = "The information could not be found")
    })
    @GetMapping("/byUser/{userId}")
    public ResponseEntity<AlbumDTO[]> getAlbumByUser(
            @PathVariable final Integer userId) {
        log.info("Connecting api with controller to get an album by user id");
        AlbumDTO[] response = albumService.getAlbumByUser(userId);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Create a new album from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_CREATED,
                message = "The album has been created correctly",
                response = AlbumDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_BAD_REQUEST,
                message = "The action could not be performed")
    })
    @PostMapping
    public ResponseEntity<AlbumDTO> createAlbum(
            @RequestBody final AlbumDTO newAlbum) {
        log.info("Connecting api with controller to get a new album");
        AlbumDTO response = albumService.createAlbum(newAlbum);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update an album from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "The album has been updated correctly",
                response = AlbumDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_BAD_REQUEST,
                message = "The action could not be performed")
    })
    @PutMapping("/{albumId}")
    public ResponseEntity<Boolean> updateAlbum(
            @PathVariable final Integer albumId,
            @RequestBody final AlbumDTO newAlbum) {
        log.info("Connecting api with controller to update an album");
        boolean response = albumService.updateAlbum(albumId, newAlbum);
        if (response == false) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete an album associated to user id from an "
            + "external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "The album has been deleted correctly",
                response = AlbumDTO.class)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<AlbumDTO> deleteAlbum(
            @PathVariable final Integer id) {
        log.info("Connecting api with controller to delete an album by id");
        albumService.deleteAlbum(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
