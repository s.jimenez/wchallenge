/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service;

import com.technical.test.wchallenge.backend.dto.AlbumDTO;

/**
 *
 * @author jhoan
 */
public interface AlbumService {

    AlbumDTO[] getAllAlbum();

    AlbumDTO[] getAlbumByUser(Integer userId);

    AlbumDTO createAlbum(AlbumDTO newAlbum);

    boolean updateAlbum(Integer albumId, AlbumDTO newAlbum);

    void deleteAlbum(Integer albumId);

}
