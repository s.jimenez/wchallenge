/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author jhoan
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "DTO class that stores the user information associated "
        + "with the permissions")
public class UserPermissionDTO {

    @ApiModelProperty(notes = "type permission associated with the user "
            + "permission")
    private String typePermission;

    @ApiModelProperty(notes = "album id associated with the user permission")
    private Integer albumId;

    @ApiModelProperty(notes = "id associated with the user permission")
    private Integer id;

    @ApiModelProperty(notes = "name associated with the user permission")
    private String name;

    @ApiModelProperty(notes = "username associated with the user permission")
    private String username;

    @ApiModelProperty(notes = "email associated with the user permission")
    private String email;

    @ApiModelProperty(notes = "DTO class address associated with the user "
            + "permission")
    private AddressDTO address;

    @ApiModelProperty(notes = "phone associated with the user permission")
    private String phone;

    @ApiModelProperty(notes = "website associated with the user permission")
    private String website;

    @ApiModelProperty(notes = "DTO class company associated with the user "
            + "permission")
    private CompanyDTO company;

}
