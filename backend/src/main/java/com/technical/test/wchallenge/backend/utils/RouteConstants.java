/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.utils;

/**
 *
 * @author jhoan
 */
public final class RouteConstants {

    //AlbumServiceImp and test
    public static final String URL_ALBUM =
            "https://jsonplaceholder.typicode.com/albums";
    public static final String URL_ALBUM_PARAM =
            "https://jsonplaceholder.typicode.com/albums?userId=";
    //CommentServiceImp and test
    public static final String URL_COMMENT =
            "https://jsonplaceholder.typicode.com/comments";
    public static final String URL_COMMENT_PARAM =
            "https://jsonplaceholder.typicode.com/comments?postId=";
    //PhotoServiceImp and test
    public static final String URL_PHOTO =
            "https://jsonplaceholder.typicode.com/photos";
    public static final String URL_PHOTO_PARAM =
            "https://jsonplaceholder.typicode.com/photos?albumId=";
    //PostServiceImp and test
    public static final String URL_POST =
            "https://jsonplaceholder.typicode.com/posts";
    public static final String URL_POST_PARAM =
            "https://jsonplaceholder.typicode.com/posts?userId=";
    //UserServiceImp and test
    public static final String URL_USER =
            "https://jsonplaceholder.typicode.com/users";

}
