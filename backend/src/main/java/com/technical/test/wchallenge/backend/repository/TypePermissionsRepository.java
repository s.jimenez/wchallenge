/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.repository;

import com.technical.test.wchallenge.backend.model.TypePermissions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jhoan
 */
@Repository
public interface TypePermissionsRepository extends 
        JpaRepository<TypePermissions, Integer>{

}
