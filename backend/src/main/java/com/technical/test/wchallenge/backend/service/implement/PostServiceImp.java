/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service.implement;

import com.technical.test.wchallenge.backend.dto.PostDTO;
import com.technical.test.wchallenge.backend.service.PostService;
import com.technical.test.wchallenge.backend.utils.RouteConstants;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author jhoan
 */
@Service
@Slf4j
public class PostServiceImp implements PostService {

    @Autowired
    private RestTemplate restTemplate;
    
    @Override
    public PostDTO[] getAllPost() {
        log.info("Entry into service function to get all posts");
        ResponseEntity<PostDTO[]> response = restTemplate
                .getForEntity(RouteConstants.URL_POST, PostDTO[].class);
        return response.getBody();
    }
    
    @Override
    public PostDTO[] getPostByUser(Integer userId) {
        log.info("Entry into service function to get a post by user id");
        ResponseEntity<PostDTO[]> response = restTemplate
                .getForEntity(RouteConstants.URL_POST_PARAM + userId,
                        PostDTO[].class);
        return response.getBody();
    }

    @Override
    public PostDTO createPost(PostDTO newPost) {
        log.info("Entry into service function to create a new post");
        ResponseEntity<PostDTO> response = restTemplate
                .postForEntity(RouteConstants.URL_POST, newPost, PostDTO.class);
        return response.getBody();
    }

    @Override
    public Boolean updatePost(Integer postId, PostDTO newPost) {
        boolean flag = false;
        log.info("Entry into service function to update a post");
        String updateURL = RouteConstants.URL_POST + "/" + newPost.getId();
        if (Objects.equals(postId, newPost.getId())){
            restTemplate.put(updateURL, newPost);
            flag = true;
        }
        return flag;
    }

    @Override
    public void deletePost(Integer postId) {
        log.info("Entry into service function to delete a post by id");
        String deleteURL = RouteConstants.URL_POST + "/" + postId;
        restTemplate.delete(deleteURL);
    }

}
