/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service;

import com.technical.test.wchallenge.backend.dto.AddressDTO;
import com.technical.test.wchallenge.backend.dto.AlbumDTO;
import com.technical.test.wchallenge.backend.dto.CompanyDTO;
import com.technical.test.wchallenge.backend.dto.GeoDTO;
import com.technical.test.wchallenge.backend.dto.UserDTO;
import com.technical.test.wchallenge.backend.dto.UserPermissionDTO;
import com.technical.test.wchallenge.backend.model.ManagementPermissions;
import com.technical.test.wchallenge.backend.model.TypePermissions;
import com.technical.test.wchallenge.backend.repository.ManagementPermissionsRepository;
import com.technical.test.wchallenge.backend.repository.TypePermissionsRepository;
import com.technical.test.wchallenge.backend.service.implement.ManagementPermissionServiceImp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author jhoan
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ManagementPermissionServiceTest {

    @Mock
    private ManagementPermissionsRepository managementPermissionsRepository;

    @Mock
    private TypePermissionsRepository typePermissionsRepository;

    @Mock
    private AlbumService albumService;

    @Mock
    private UserService userService;

    @InjectMocks
    private ManagementPermissionServiceImp managementPermissionService;

    @Test
    public void testCreatePermission() {
        Integer[] listType = new Integer[1];
        ArrayList<Integer> arrayType = new ArrayList<>();
        Integer newTypeId = 1;
        arrayType.add(newTypeId);
        arrayType.toArray(listType);
        List<ManagementPermissions> arrayPermission = new ArrayList<>();
        ManagementPermissions newPermission = new ManagementPermissions(1, 1,
                1, listType);
        AlbumDTO[] listAlbum = new AlbumDTO[1];
        ArrayList<AlbumDTO> arrayAlbum = new ArrayList<>();
        AlbumDTO newAlbum = new AlbumDTO(1, 1, "nn");
        arrayPermission.add(newPermission);
        arrayAlbum.add(newAlbum);
        arrayAlbum.toArray(listAlbum);
        when(albumService.getAlbumByUser(newPermission.getAlbumId()))
                .thenReturn(listAlbum);
        when(managementPermissionsRepository.findAll()).thenReturn(
                arrayPermission);
        when(managementPermissionsRepository.save(newPermission))
                .thenReturn(newPermission);
        ManagementPermissions permission = managementPermissionService
                .createPermissions(newPermission);
        assertNull(permission);
    }
    
    @Test
    public void testCreatePermissionElse() {
        Integer[] listTypeId = new Integer[1];
        ArrayList<Integer> arrayType = new ArrayList<>();
        Integer newTypeId = 2;
        arrayType.add(newTypeId);
        arrayType.toArray(listTypeId);
        List<ManagementPermissions> arrayPermission = new ArrayList<>();
        ManagementPermissions permissionToArray = new ManagementPermissions(1,
                1, 2, listTypeId);
        ManagementPermissions newPermission = new ManagementPermissions(1, 1,
                1, listTypeId);
        AlbumDTO[] listAlbum = new AlbumDTO[1];
        ArrayList<AlbumDTO> arrayAlbum = new ArrayList<>();
        AlbumDTO newAlbum = new AlbumDTO(1, 2, "nn");
        arrayPermission.add(permissionToArray);
        arrayAlbum.add(newAlbum);
        arrayAlbum.toArray(listAlbum);
        when(albumService.getAlbumByUser(newPermission.getUserId()))
                .thenReturn(listAlbum);
        when(managementPermissionsRepository.findAll()).thenReturn(
                arrayPermission);
        when(managementPermissionsRepository.save(newPermission))
                .thenReturn(newPermission);
        ManagementPermissions permission = managementPermissionService
                .createPermissions(newPermission);
        assertNotNull(permission);
        assertNotEquals(permission.getAlbumId(), 
                permissionToArray.getAlbumId());
    }

    @Test
    public void testUpdatePermissionByUser() {
        Integer[] listTypeId = new Integer[1];
        ArrayList<Integer> arrayType = new ArrayList<>();
        Integer newTypeId = 1;
        arrayType.add(newTypeId);
        arrayType.toArray(listTypeId);
        Optional<ManagementPermissions> permission = Optional
                .of(new ManagementPermissions(1, 1, 1, listTypeId));
        when(managementPermissionsRepository.findById(newTypeId))
                .thenReturn(permission);
        when(managementPermissionsRepository.save(permission.get()))
                .thenReturn(permission.get());
        ManagementPermissions result = managementPermissionService
                .updatePermissionByUser(newTypeId, permission.get());
        assertEquals(result.getId(), permission.get().getId());
    }

    @Test
    public void testGetAllUserByAlbumPermission() {
        Integer[] listTypeId = new Integer[1];
        ArrayList<Integer> arrayType = new ArrayList<>();
        Integer localId = 1;
        arrayType.add(localId);
        arrayType.toArray(listTypeId);
        List<ManagementPermissions> arrayPermission = new ArrayList<>();
        ManagementPermissions newPermission = new ManagementPermissions(1, 1,
                1, listTypeId);
        arrayPermission.add(newPermission);
        UserDTO user = new UserDTO(1, "nn", "nn", "nn", null, "nn", "nn", null);
        CompanyDTO company = new CompanyDTO("nn", "nn", "nn");
        GeoDTO geo = new GeoDTO("nn", "nn");
        AddressDTO address = new AddressDTO("nn", "nn", "nn", "nn", geo);
        UserPermissionDTO updateUserPermission = new UserPermissionDTO("nn",
                1, 1, "nn", "nn", "nn", address, "nn", "nn", company);
        UserPermissionDTO setUserPermission = new UserPermissionDTO();
        setUserPermission.setTypePermission(updateUserPermission
                .getTypePermission());
        setUserPermission.setAlbumId(updateUserPermission.getAlbumId());
        setUserPermission.setAddress(updateUserPermission.getAddress());
        setUserPermission.setCompany(updateUserPermission.getCompany());
        setUserPermission.setEmail(updateUserPermission.getEmail());
        setUserPermission.setId(updateUserPermission.getId());
        setUserPermission.setName(updateUserPermission.getName());
        setUserPermission.setPhone(updateUserPermission.getPhone());
        setUserPermission.setUsername(updateUserPermission.getUsername());
        setUserPermission.setWebsite(updateUserPermission.getWebsite());
        Optional<TypePermissions> permission = Optional.of(
                new TypePermissions(1, "write"));
        when(managementPermissionsRepository.findAll()).thenReturn(
                arrayPermission);
        when(userService.getUserById(localId)).thenReturn(user);
        when(typePermissionsRepository.findById(localId))
                .thenReturn(permission);
        ArrayList<UserPermissionDTO> result = managementPermissionService
                .getAllUserByAlbumPermission(localId, localId);
        assertEquals(result.size(), arrayPermission.size());
    }
    
    @Test
    public void testDeletePermission() {
        Integer localId = 1;
        Integer[] listType = new Integer[1];
        ArrayList<Integer> arrayType = new ArrayList<>();
        arrayType.add(localId);
        arrayType.toArray(listType);
        managementPermissionsRepository.deleteById(localId);
        ArrayList<ManagementPermissions> listPermission = new ArrayList<>();
        ManagementPermissions newPermission = new ManagementPermissions(1, 1,
                1, listType);
        listPermission.add(newPermission);
        when(managementPermissionsRepository.findAll()).thenReturn(
                listPermission);
        ArrayList<ManagementPermissions> result = managementPermissionService
                .deletePermission(localId);
        assertEquals(result.size(), listPermission.size());
    }
    
    @Test
    public void testCreateTypePermissions() {
        TypePermissions typePermission = new TypePermissions(1, "write");
        TypePermissions setPermission = new TypePermissions();
        setPermission.setId(typePermission.getId());
        setPermission.setType(typePermission.getType());
        when(typePermissionsRepository.save(typePermission))
                .thenReturn(typePermission);
        TypePermissions result = managementPermissionService
                .createTypePermissions(typePermission);
        assertEquals(result.getId(), typePermission.getId());
    }
    
    @Test
    public void testGetAllTypePermissions() {
        ArrayList<TypePermissions> arrayType = new ArrayList<>();
        TypePermissions typePermission = new TypePermissions(1, "write");
        arrayType.add(typePermission);
        when(typePermissionsRepository.findAll()).thenReturn(arrayType);
        ArrayList<TypePermissions> result = managementPermissionService
                .getAllTypePermissions();
        assertEquals(result.size(), arrayType.size());
    }
}
