/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.controller;

import com.technical.test.wchallenge.backend.dto.PhotoDTO;
import com.technical.test.wchallenge.backend.service.PhotoService;
import com.technical.test.wchallenge.backend.utils.HttpConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@RequestMapping(value = "/api/v1/photos")
@Api(value = "Management of photos resources",
        description = "Receive all photo information from an external api")
@Slf4j
public class PhotoController {

    @Autowired
    private PhotoService photoService;

    @ApiOperation(value = "Get all photo information from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Photo information found", response = PhotoDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_NOT_FOUND,
                message = "The information could not be found")
    })
    @GetMapping
    public ResponseEntity<PhotoDTO[]> getAllPhotos() {
        log.info("Connecting api with controller to get all photos");
        PhotoDTO[] response = photoService.getAllPhotos();
        if (response == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Get all photo information associated with the user "
            + "id from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Photo information found", response = PhotoDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_NOT_FOUND,
                message = "The information could not be found")
    })
    @GetMapping("/{userId}")
    public ResponseEntity<ArrayList<PhotoDTO>> getPhotosByUser(
            @PathVariable final Integer userId) {
        log.info("Connecting api with controller to get all photos by user id");
        ArrayList<PhotoDTO> response = photoService.getPhotosByUser(userId);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Create a new photo information from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_CREATED, 
                message = "Photo information has been created correctly",
                response = PhotoDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_BAD_REQUEST, 
                message = "The action could not be performed")
    })
    @PostMapping
    public ResponseEntity<PhotoDTO> createPhoto(
            @RequestBody final PhotoDTO newPhoto) {
        log.info("Connecting api with controller to create a photo");
        PhotoDTO response = photoService.createPhoto(newPhoto);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update a photo information from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Photo information has been updated correctly",
                response = PhotoDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_BAD_REQUEST, 
                message = "The action could not be performed")
    })
    @PutMapping("/{photoId}")
    public ResponseEntity<PhotoDTO> updatePhoto(
            @PathVariable final Integer photoId,
            @RequestBody final PhotoDTO newPhoto) {
        log.info("Connecting api with controller to update a photo");
        Boolean response = photoService.updatePhoto(photoId, newPhoto);
        if (response == false) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Delete a photo information associated with the id"
            + " from an external api")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Photo information has been deleted correctly",
                response = PhotoDTO.class)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<PhotoDTO> deletePhoto(
            @PathVariable final Integer id) {
        log.info("Connecting api with controller to delete a photo by id");
        photoService.deletePhoto(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
