/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.service;

import com.technical.test.wchallenge.backend.dto.UserPermissionDTO;
import com.technical.test.wchallenge.backend.model.ManagementPermissions;
import com.technical.test.wchallenge.backend.model.TypePermissions;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jhoan
 */
public interface ManagementPermissionService {

    TypePermissions createTypePermissions(TypePermissions types);

    List<TypePermissions> getAllTypePermissions();

    ManagementPermissions createPermissions(ManagementPermissions permission);

    List<ManagementPermissions> getAllPermissions();

    ManagementPermissions updatePermissionByUser(Integer permissionId,
            ManagementPermissions updatePermission);

    ArrayList<UserPermissionDTO> getAllUserByAlbumPermission(
            Integer typePermissionId, Integer albumId);
    
    ArrayList<ManagementPermissions> deletePermission(Integer permissionId);

}
