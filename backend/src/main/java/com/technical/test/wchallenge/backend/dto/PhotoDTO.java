/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author jhoan
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "DTO class that stores the photo information")
public class PhotoDTO {

    @ApiModelProperty(notes = "album id associated with the photo")
    private Integer albumId;

    @ApiModelProperty(notes = "id associated with the photo")
    private Integer id;

    @ApiModelProperty(notes = "title associated with the photo")
    private String title;

    @ApiModelProperty(notes = "url associated with the photo")
    private String url;

    @ApiModelProperty(notes = "thumbnail url associated with the photo")
    private String thumbnailUrl;

}
