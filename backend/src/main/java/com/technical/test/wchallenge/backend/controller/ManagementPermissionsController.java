/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.controller;

import com.technical.test.wchallenge.backend.dto.UserPermissionDTO;
import com.technical.test.wchallenge.backend.model.ManagementPermissions;
import com.technical.test.wchallenge.backend.model.TypePermissions;
import com.technical.test.wchallenge.backend.service.ManagementPermissionService;
import com.technical.test.wchallenge.backend.utils.HttpConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@RequestMapping(value = "/api/v1/permissions")
@Api(value = "Management of permissions resources",
        description = "Receive all permissions information from our database")
@Slf4j
public class ManagementPermissionsController {

    @Autowired
    private ManagementPermissionService managementPermissionService;

    @ApiOperation(value = "Get all type permission information from our "
            + "database")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Type permission information found",
                response = TypePermissions.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_NOT_FOUND,
                message = "The information could not be found")
    })
    @GetMapping("/types")
    public ResponseEntity<List<TypePermissions>> getAllTypesPermissions() {
        log.info("Connecting api with controller to get all types of "
                + "permissions");
        List<TypePermissions> response = managementPermissionService
                .getAllTypePermissions();
        if (response == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Create a new type permission information to add it"
            + " to our database")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_CREATED,
                message = "Type permission information has been created",
                response = TypePermissions.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_BAD_REQUEST,
                message = "The action could not be performed")
    })
    @PostMapping("/types")
    public ResponseEntity<TypePermissions> createTypesPermissions(
            @RequestBody final TypePermissions types) {
        log.info("Connecting api with controller to create a new types of "
                + "permissions");
        TypePermissions response = managementPermissionService
                .createTypePermissions(types);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Get all permission information from our "
            + "database")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Permission information found",
                response = ManagementPermissions.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_NOT_FOUND,
                message = "The information could not be found")
    })
    @GetMapping
    public ResponseEntity<List<ManagementPermissions>> getAllPermissions() {
        log.info("Connecting api with controller to get all permissions");
        List<ManagementPermissions> response = managementPermissionService
                .getAllPermissions();
        if (response == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Get all user permission information from our "
            + "database")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "User permission information found",
                response = UserPermissionDTO.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_NOT_FOUND, 
                message = "The information could not be found")
    })
    @GetMapping("/users")
    public ResponseEntity<ArrayList<UserPermissionDTO>>
            getAllUserByAlbumPermission(
                    @RequestParam final Integer typePermissionId,
                    @RequestParam final Integer albumId) {
        log.info("Connecting api with controller to get all users associated "
                + "with the albums permissions");
        ArrayList<UserPermissionDTO> response = managementPermissionService
                .getAllUserByAlbumPermission(typePermissionId, albumId);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Update a type permission information to add it"
            + " to our database")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Permission has been updated to our database",
                response = ManagementPermissions.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_BAD_REQUEST,
                message = "The action could not be performed")
    })
    @PutMapping("/{userId}")
    public ResponseEntity<ManagementPermissions>
            updatePermissionsByUser(@PathVariable final Integer userId,
                    @RequestBody final ManagementPermissions updatePermission) {
        log.info("Connecting api with controller to update a permission");
        ManagementPermissions response = managementPermissionService
                .updatePermissionByUser(userId, updatePermission);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete a permission information associated with the "
            + "id from our database")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_OK,
                message = "Permission has been deleted of our database",
                response = ManagementPermissions.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_BAD_REQUEST,
                message = "The action could not be performed")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<List<ManagementPermissions>> deletePermission(
            @PathVariable final Integer id) {
        log.info("Connecting api with controller to delete a permission");
        List<ManagementPermissions> response = managementPermissionService
                .deletePermission(id);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Create a new permission information to add it"
            + " to our database")
    @ApiResponses(value = {
        @ApiResponse(code = HttpConstants.HTTP_STATUS_CREATED,
                message = "Permission has been created to our database",
                response = ManagementPermissions.class),
        @ApiResponse(code = HttpConstants.HTTP_STATUS_BAD_REQUEST,
                message = "The action could not be performed")
    })
    @PostMapping
    public ResponseEntity<ManagementPermissions> createPermissions(
            @RequestBody final ManagementPermissions permission) {
        log.info("Connecting api with controller to create a new permission");
        ManagementPermissions response = managementPermissionService
                .createPermissions(permission);
        if (response == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

}
