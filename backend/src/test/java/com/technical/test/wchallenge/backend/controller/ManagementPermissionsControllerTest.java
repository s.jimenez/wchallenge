/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.controller;

import com.technical.test.wchallenge.backend.dto.UserPermissionDTO;
import com.technical.test.wchallenge.backend.model.ManagementPermissions;
import com.technical.test.wchallenge.backend.model.TypePermissions;
import com.technical.test.wchallenge.backend.service.ManagementPermissionService;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author jhoan
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ManagementPermissionsControllerTest {

    @Mock
    private ManagementPermissionService permissionService;

    @InjectMocks
    private ManagementPermissionsController permissionController;

    @Test
    public void testGetAllTypesPermissionsNotFound() {
        when(permissionService.getAllTypePermissions()).thenReturn(null);
        ResponseEntity<List<TypePermissions>> commentCon = permissionController
                .getAllTypesPermissions();
        assertNotNull(commentCon);
    }

    @Test
    public void testGetAllTypesPermissionsOk() {
        ArrayList<TypePermissions> arrayType = new ArrayList<>();
        TypePermissions newType = new TypePermissions(1, "sebas");
        arrayType.add(newType);
        when(permissionService.getAllTypePermissions()).thenReturn(arrayType);
        ResponseEntity<List<TypePermissions>> permissionCon
                = permissionController.getAllTypesPermissions();
        assertEquals(permissionCon.getBody().size(), arrayType.size());
    }

    @Test
    public void testCreateTypesPermissionsNull() {
        TypePermissions newType = new TypePermissions();
        when(permissionService.createTypePermissions(newType)).thenReturn(null);
        ResponseEntity<TypePermissions> permissionCon = permissionController
                .createTypesPermissions(newType);
        assertNull(permissionCon.getBody());
    }

    @Test
    public void testCreateTypesPermissionsNotNull() {
        TypePermissions newType = new TypePermissions(1, "sebas");
        when(permissionService.createTypePermissions(newType))
                .thenReturn(newType);
        ResponseEntity<TypePermissions> permissionCon = permissionController
                .createTypesPermissions(newType);
        assertEquals(permissionCon.getBody(), newType);
    }

    @Test
    public void testGetAllPermissionsNotFound() {
        when(permissionService.getAllPermissions()).thenReturn(null);
        ResponseEntity<List<ManagementPermissions>> permissionCon
                = permissionController.getAllPermissions();
        assertNotNull(permissionCon);
    }

    @Test
    public void testGetAllPermissionsOk() {
        ArrayList<ManagementPermissions> arrayPermission = new ArrayList<>();
        ManagementPermissions newPermission = new ManagementPermissions(1, 1, 1,
                null);
        arrayPermission.add(newPermission);
        when(permissionService.getAllPermissions()).thenReturn(arrayPermission);
        ResponseEntity<List<ManagementPermissions>> permissionCon
                = permissionController.getAllPermissions();
        assertEquals(permissionCon.getBody().size(), arrayPermission.size());
    }

    @Test
    public void testGetAllUserByAlbumPermissionNotFound() {
        Integer albumId = 1;
        Integer typeId = 1;
        when(permissionService.getAllUserByAlbumPermission(typeId, albumId))
                .thenReturn(null);
        ResponseEntity<ArrayList<UserPermissionDTO>> permissionCon
                = permissionController.getAllUserByAlbumPermission(typeId,
                        albumId);
        assertNotNull(permissionCon);
    }

    @Test
    public void testGetAllUserByAlbumPermissionOk() {
        Integer albumId = 1;
        Integer typeId = 1;
        ArrayList<UserPermissionDTO> arrayPermission = new ArrayList<>();
        UserPermissionDTO newPermission = new UserPermissionDTO("nn", 1, 1,
                "nn", "nn", "nn", null, "nn", "nn", null);
        arrayPermission.add(newPermission);
        when(permissionService.getAllUserByAlbumPermission(typeId, albumId))
                .thenReturn(arrayPermission);
        ResponseEntity<ArrayList<UserPermissionDTO>> permissionCon
                = permissionController.getAllUserByAlbumPermission(typeId,
                        albumId);
        assertNotNull(permissionCon);
        assertEquals(permissionCon.getBody().size(), arrayPermission.size());
    }

    @Test
    public void testUpdatePermissionsByUserNull() {
        Integer userId = 1;
        ManagementPermissions updatePermission = new ManagementPermissions();
        when(permissionService.updatePermissionByUser(userId, updatePermission))
                .thenReturn(null);
        ResponseEntity<ManagementPermissions> permissionCon
                = permissionController.updatePermissionsByUser(userId,
                        updatePermission);
        assertNull(permissionCon.getBody());
    }

    @Test
    public void testUpdatePermissionsByUserNotNull() {
        Integer userId = 1;
        ManagementPermissions updatePermission = new ManagementPermissions(1, 1,
                1, null);
        when(permissionService.updatePermissionByUser(userId, updatePermission))
                .thenReturn(updatePermission);
        ResponseEntity<ManagementPermissions> permissionCon = 
                permissionController.updatePermissionsByUser(userId, 
                        updatePermission);
        assertEquals(permissionCon.getBody(), updatePermission);
    }

    @Test
    public void testDeletePermissionsByIdNotFound() {
        Integer permissionId = 1;
        when(permissionService.deletePermission(permissionId))
                .thenReturn(null);
        ResponseEntity<List<ManagementPermissions>> permissionCon
                = permissionController.deletePermission(permissionId);
        assertNotNull(permissionCon);
    }

    @Test
    public void testDeletePermissionsByIdOk() {
        Integer permissionId = 1;
        ArrayList<ManagementPermissions> arrayPermission = new ArrayList<>();
        ManagementPermissions newPermission = new ManagementPermissions(1, 1,
                1, null);
        arrayPermission.add(newPermission);
        when(permissionService.deletePermission(permissionId))
                .thenReturn(arrayPermission);
        ResponseEntity<List<ManagementPermissions>> permissionCon
                = permissionController.deletePermission(permissionId);
        assertNotNull(permissionCon);
        assertEquals(permissionCon.getBody().size(), arrayPermission.size());
    }

    @Test
    public void testCreatePermissions() {
        ManagementPermissions newPermission = new ManagementPermissions();
        when(permissionService.createPermissions(newPermission))
                .thenReturn(null);
        ResponseEntity<ManagementPermissions> permissionCon =
                permissionController.createPermissions(newPermission);
        assertNull(permissionCon.getBody());
    }

    @Test
    public void testCreateCommentNotNull() {
        ManagementPermissions newPermission = new ManagementPermissions(1, 1, 1,
                null);
        when(permissionService.createPermissions(newPermission))
                .thenReturn(newPermission);
        ResponseEntity<ManagementPermissions> permissionCon =
                permissionController.createPermissions(newPermission);
        assertEquals(permissionCon.getBody(), newPermission);
    }
}
