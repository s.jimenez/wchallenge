/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technical.test.wchallenge.backend.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author jhoan
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "DTO class that stores the address information")
public class AddressDTO {

    @ApiModelProperty(notes = "street associated with the address")
    private String street;

    @ApiModelProperty(notes = "suite associated with the address")
    private String suite;

    @ApiModelProperty(notes = "city associated with the address")
    private String city;

    @ApiModelProperty(notes = "zipcode associated with the address")
    private String zipcode;

    @ApiModelProperty(notes = "geo associated with the address")
    private GeoDTO geo;
    
}
